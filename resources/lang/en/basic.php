<?php

return [

    'add_user' => 'Add User',
    'add_track' => 'Add Track',
    'edit_track' => 'Edit Track',
    'add' => 'Add',
    'save' => 'Save',
    'help' => 'Help',
    'copy' => 'Copy',
    'goto' => 'Go to',
    'edit' => 'Edit',
    'remove' => 'Remove',
    'title' => 'Title',
    'start' => 'Start',
    'stop' => 'Stop',
    'lat' => 'Lat',
    'lon' => 'Lon',
    'radius' => 'Radius',
    'timer' => 'Timer',
    'loading' => 'Loading...',
    'nothing_to_show' => 'Nothing to show',
    'date_and_time'=>'Date & Time',
    'close'=>'Close',
    'delete_track_cnf'=>'Are you sure you want to delete',
    'delete_user_cnf' => 'Are you sure you want to delete user',
    'no_points' => 'No Points'
];
