<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    public $timestamps = false;

    public function track()
    {
        return $this->belongsTo('App\Track');
    }
}
