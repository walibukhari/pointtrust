<?php

return [

'add_user' => 'Voeg gebruiker toe',
'add_track' => 'Track toevoegen',
'edit_track' => 'Track bewerken',
'add' => 'Toevoegen',
'save' => 'Opslaan',
'help' => 'Helpen',
'copy' => 'Kopiëren',
'goto' => 'Ga naar',
'edit' => 'Bewerk',
'remove' => 'Verwijderen',
'title' => 'Titel',
'start' => 'Begin',
'stop' => 'Hou op',
'lat' => 'Bre',
'lon' => 'Len',
'radius' => 'Straal',
'timer' => 'Timer',
'loading' => 'Bezig met laden...',
'nothing_to_show' => 'niets te laten zien',
'date_and_time'=>'Datum & Tijd',
'close'=>'Dichtbij',
'delete_track_cnf'=>'Weet je zeker dat je wilt verwijderen',
'delete_user_cnf' => 'Weet u zeker dat u de gebruiker wilt verwijderen',
'no_points' => 'Geen punten'
];
