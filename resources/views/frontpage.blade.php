<!DOCTYPE html>
<html>
<head>
    <title>{{$track->title}}</title>

   
  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/css/app.css?11" />

</head>
<body>
<div id="map"></div>
<div id="countdown"><span class="left"><i class="fas fa-bars disc"></i></i></span>
    {{-- <span id="clock" class="right"></span> --}}
    Pointrush
</div>
<div id="disclaimer">
    <div class="header">
        <h3 class="left">Disclaimer</h3>
    <span class="right"><i class="fas fa-times disc" ></i></span>
</div>
   <div class="text">


   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.


</div> 
</div>
<script src="/js/app.js?11" type="text/javascript"></script>
</body>
</html>
