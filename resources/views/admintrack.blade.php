@extends('layouts.app')

@section('content')

<div id="map" style="min-height:500px;height:50%"></div>

<div style="height:50%;overflow-y: scroll;">
    <div class="container">
        
        <div class="row my-3">
            <div class="col-md-6">
                <button id="add" class="btn btn-default">{{ __('basic.add') }}</button>
        <button id="save" class="btn btn-default">{{ __('basic.save') }}</button>
            </div>
            <div class="col-md-1">
                <button href="/admin/help" target="_blank" class="btn btn-default" onClick="window.open('/admin/help', '_blank', 'toolbar=0,location=0,menubar=0');">{{ __('basic.help') }}</button>
            </div>
            <div class="col-md-5">
                <div class="input-group">
                    <a href="{{URL::to('/track/'.$id)}}" class="btn btn-default" type="button" id="link-button"
                          data-toggle="tooltip" data-placement="bottom"
                          title="{{ __('basic.goto') }}" target="_blank">
                          <i class="fas fa-link"></i>
                </a>
                    <input type="text" class="form-control"
                        value="{{URL::to('/track/'.$id)}}" placeholder="Some path" id="copy-input">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button" id="copy-button"
                          data-toggle="tooltip" data-placement="bottom"
                          title="{{ __('basic.copy') }}">
                          <i class="fas fa-copy"></i>
                      </button>
                    </span>
                  </div>
            </div>
        </div>
        <div id="data-table">

        </div>


    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('basic.date_and_time') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="overflow:hidden;">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="datetimepicker13"></div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">

                    </script>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('basic.close') }}</button>

            </div>
        </div>
    </div>
</div>

<div id="disclaimer">
    <div class="header">
        <h3 class="left">Disclaimer</h3>
        <span class="right"><i class="fas fa-times disc"></i></span>
    </div>
    <div class="text">


        Voor vragen over de Admin pagina, vraag Arjan</BR>
        Hij is te bereiken op 0643 23 13 03
    </div>
</div>

@endsection
