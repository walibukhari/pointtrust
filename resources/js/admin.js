require('./bootstrap');
// require('./tracktable');
require('leaflet');


require('jquery-countdown');
global.moment=require('moment');






require('tempusdominus-bootstrap-4');
$.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
    icons: {
        time: 'fas fa-clock',
        date: 'fas fa-calendar',
        up: 'fas fa-arrow-up',
        down: 'fas fa-arrow-down',
        previous: 'fas fa-chevron-left',
        next: 'fas fa-chevron-right',
        today: 'fas fa-calendar-check-o',
        clear: 'fas fa-trash',
        close: 'fas fa-times'
    } });
require('moment-timezone');
var table;
// var z="+0300";
var Tabulator = require('tabulator-tables');
$(document).ready(function () {

    var del = ',';
    var circle = [];
    var green = {
        color: "#00ff00",
        fillColor: "#00ff00",
        fillOpacity: 0.5
    };
    var blue = {
        color: "#3388ff",
        fillColor: "#3388ff",
        fillOpacity: 0.5
    };
    var yellow = {
        color: "#ffff33",
        fillColor: "#ffff33",
        fillOpacity: 0.5
    };

    var selected;
    var befselected = 0;
    //////console.log('showing map');
    if ($('#map').length) {
        var map = L.map('map', {
            center: [52.144681, 6.394280],
            zoom: 10,
            zoomControl: false
        });
        var theMarker = {};
        map.on('click',
            function mapClickListen(e) {
                var pos = e.latlng;
                var lat = e.latlng.lat;
                var lon = e.latlng.lng;
                console.log('map click event');
                var marker = L.marker(
                    pos, {
                        draggable: true
                    }
                );
                marker.on('drag', function (e) {
                    console.log('marker drag event');
                });
                marker.on('dragstart', function (e) {
                    console.log('marker dragstart event');
                    map.off('click', mapClickListen);
                });
                marker.on('dragend', function (e) {
                    var marker = e.target;
                    var position = marker.getLatLng();
                    console.log('marker and position');
                    console.log(position.lat);
                    console.log(position.lng);
                    $('.tabulator-cell').each(function (e, val) {
                        console.log('tabulator-cell');
                        console.log(e);
                        if(e == 4){
                            console.log($('.tabulator-cell').attr('tabulator-field','lat'));
                                let lat = $('.tabulator-cell').attr('tabulator-field','lat')[4].innerText = position.lat;
                                console.log(lat);
                        }
                        if(e == 5){
                            console.log($('.tabulator-cell').attr('tabulator-field','lon'));
                                let lon = $('.tabulator-cell').attr('tabulator-field','lon')[5].innerText = position.lng;
                                console.log(lon);
                        }
                    });
                    console.log('marker dragend event');
                    setTimeout(function () {
                        map.on('click', mapClickListen);
                    }, 10);
                });
                if (theMarker != undefined) {
                    map.removeLayer(theMarker);
                }
                ;
                //Add a marker to show where you clicked.
                theMarker = marker.addTo(map);
            }
        );
        L.control.zoom({
            position: 'bottomright'
        }).addTo(map);
        L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        //////console.log('tiles loaded');
    }
    var cGroup = null;


    // $.get('config.csv', function (data) {
    getpoint();


    $('#add').click(function () {
        var curdata = table.getData();
        ////console.log("ll",curdata);
        console.log('curdata.length');
        if (curdata.length) {
            var nid = curdata[curdata.length - 1].id + 1;
        } else {
            var nid = 1;
        }

        table.addData([{
            id: nid,
            loc: false,
            lat: "52.144681",
            lon: "6.394280",
            radius: "",
            title: "",
            start: "",
            stop: "",
            time: false
        }], false);
    });
    $('#save').click(function () {

        tabledata = table.getData();
        //console.log("ccc",tabledata)
        $.ajax({
            type: "POST",
            url: "/admin/savepoints/" + location.href.substring(location.href.lastIndexOf('/') + 1),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                data: tabledata
            },


        }).done(function () {
            alert("success");
        })
            .fail(function (jqXHR, textStatus) {
                alert("error " + textStatus);
            });

    });
    $('.disc').click(function () {

        $('#disclaimer').toggle();
    });

    function getpoint() {
        //  ////console.log('getting point');

        $.get({
            url: '/admin/getpoints/'+location.href.substring(location.href.lastIndexOf('/') + 1),
            cache: 'false'
        }).then(function (data) {
            //////console.log('got point: '+data);
            var d = convertData(data);
            //var d=data.data;
            ////console.log('got point: ',d);
            //   ////console.log('switching cirle');
            switchCircle(d);

////console.log ('building table');
            if($("#data-table").length){
                table = new Tabulator("#data-table", {
                    layout: "fitColumns",
                    selectable: 1,
                    data: d,
                    columns: [{
                        title: "ID",
                        field: "id"
                    },{
                        title: trans('basic.title'),
                        field: "title",
                        editor: true
                    },
                        {
                            title: trans('basic.start'),
                            field: "start",

                            editor:dateEditor
                        },
                        {
                            title: trans('basic.stop'),
                            field: "stop",
                            editor:dateEditor
                        },
                        {
                            title: trans('basic.lat'),
                            field: "lat",
                        },
                        {
                            title: trans('basic.lon'),
                            field: "lon",
                        },
                        {
                            title: trans('basic.radius'),
                            field: "radius",
                            editor: true
                        },
                        {
                            title:trans('basic.timer'),
                            field:"time",
                            align:"center",
                            editor:true,
                            formatter:"tickCross"
                        },
                        {
                            title: '',
                            headerSort:false,
                            formatter: function(cell, formatterParams, onRendered){return '<i class="fas fa-copy"></i>';},
                            width: 30,
                            align: "center",
                            cellClick: function (e, cell) {
                                console.log("copy",cell._cell.row.data.lat);
                                var curdata=table.getData();
                                console.log("ll",curdata);
                                if(curdata.length)
                                {
                                    var nid=curdata[curdata.length-1].id+1;
                                }
                                else
                                {
                                    var nid=1;
                                }
                                // ////console.log(curdata);
                                ////console.log(nid);
                                table.addData([{
                                    id:nid,
                                    start:cell._cell.row.data.start,
                                    stop:cell._cell.row.data.stop,
                                    loc:cell._cell.row.data.loc,
                                    lat:cell._cell.row.data.lat,
                                    lon:cell._cell.row.data.lon,
                                    radius:cell._cell.row.data.radius,
                                    title:cell._cell.row.data.title,
                                    time:cell._cell.row.data.time
                                }], false);
                            }
                        },
                        {
                            title: "",
                            headerSort:false,
                            formatter: "buttonCross",
                            width: 30,
                            align: "center",
                            cellClick: function (e, cell) {
                                table.deleteRow(cell.getRow().getData().id);
                            }
                        }

                    ],
                    rowSelectionChanged: function (data, rows) {
                        // ////console.log(circle);
                        $.each(circle, function (ind, val) {
                            if (circle[ind]) {
                                console.log(circle[ind]._radius);
                                if(circle[ind]._radius)
                                {
                                    this.setStyle(blue);
                                }
                            }



                        });

                        if (data.length != 0) {
                            console.log("circle");
                            console.log(data);
                            console.log(circle);
//////console.log(circle);


                            if (circle[data[0].id]) {



                                selected = data[0];
                                if(circle[data[0].id]._radius){
                                    circle[data[0].id].bringToFront();
                                    circle[data[0].id].setStyle(yellow);
                                }
                                var nrad = 20 - Math.log(data[0].radius);
                                map.setView([data[0].lat, data[0].lon], nrad);
                            }
                        }

                        // ////console.log(data);
                        //  ////console.log(rows);
                    },
                    dataEdited: function (data) {
                        //   ////console.log(data);


                        $.each(data, function (ind, val) {


                            if (this.lat == '' || this.lon == '') {
                                table.updateData([{
                                    id: (ind + 1),
                                    loc: false
                                }]);
                            } else {
                                table.updateData([{
                                    id: (ind + 1),
                                    loc: true
                                }]);
                            }
                        });
                        //  ////console.log(data);

                        switchCircle(data);
                    },
                    rowDeleted:function(row){
                        ////console.log(table.getData());
                        switchCircle(table.getData());
                    },
                });
            }
        });
    }


    function convertData(data) {


        var res = [];
        // var row = data.split('\n');
        var row = data.data;
        $.each(row, function (ind, val) {
            //   ////console.log("tl "+this.length);
            //   ////console.log(this);
            if (this.length != 0) {
                var obj = new Object;
                //var cols = this.split(del);
                obj.stop = moment(this.stop + " "+z, "YYYY-MM-DD hh:mm:ss Z").format("DD-MM-YYYY HH:mm:ss");
                obj.start = moment(this.start + " "+z, "YYYY-MM-DD hh:mm:ss Z").format("DD-MM-YYYY HH:mm:ss");
                obj.title = this.title;
                obj.time = this.time;
                //    ////console.log(cols[1]);
                //var coord = cols[1].split('/');
                if (this.lat.length && this.lon.length) {
                    obj.lat = this.lat;
                    obj.lon = this.lon;
                    obj.loc = true;
                } else {
                    obj.loc = false;
                    obj.lat ="";
                    obj.lon = "";
                }
                obj.radius = this.radius;
                obj.id = ind + 1;
                res.push(obj);
            }
        });


        return res;
    }
    function switchCircle(d) {

        // ////console.log('x');
        $.each(circle, function (ind, val) {
            map.removeLayer(this);

        });
        $.each(d, function (ind, val) {
            ////console.log("this.loc",this.loc);
            //to del
            //this.loc = true
            // if (this.lat == '' || this.lon == '') {
            //     this.loc == false;
            // } else {
            //     this.loc == true;
            // }


            //cGroup[ind] = L.layerGroup().addTo(map);
            if (this.loc) {
                if (circle[this.id]) {


                    map.removeLayer(circle[this.id]);
                    $('.my-div-icon-'+ind).remove();
                }
                ////console.log(this.lat, this.lon,this.radius);
                if(this.radius==1)
                {
                    circle[this.id] = L.marker([this.lat, this.lon]).addTo(map);
                }
                else{
                    circle[this.id] = L.circle([this.lat, this.lon], {
                        radius: this.radius,
                        myid: this.id
                    }).addTo(map);
                }
                circle[this.id].bindPopup("Rij: " + this.id + "<br/>Title:" + this.title + "<br/>Start: " + moment(this.start + " "+z, "DD-MM-YYYY hh:mm:ss Z").toDate() +"<br/>Stop: " + moment(this.stop + " "+z, "DD-MM-YYYY hh:mm:ss Z").toDate() +  "<br/> Lat: " + this.lat + "<br/> Lon: " + this.lon + "<br/> Radius: " + this.radius + " meters");
                //circle[this.id].bindPopup("Rij: " + this.id + "<br/>Title:" + this.title + "<br/>Start: " + moment(this.start + " "+z, "YYYY-MM-DD hh:mm:ss Z").toDate() +"<br/>Stop: " + moment(this.stop + " "+z, "YYYY-MM-DD hh:mm:ss Z").toDate() +  "<br/> Lat: " + this.lat + "<br/> Lon: " + this.lon + "<br/> Radius: " + this.radius + " meters");

                //   circle[ind].on('mouseover', function (e) {
                //      this.bringToFront();
                //  });
                //  circle[ind].on('mouseout', function (e) {
                //     this.bringToFront();
                //  });

                circle[this.id].on('click', function (e) {
                    this.openPopup();
                    // ////console.log(e);
                    //  ////console.log(this);
                    table.selectRow(this.options.myid);
                    //  ////console.log(this.options.myid);
                    //  ////console.log(selected.id);
                    if(befselected==this.options.myid)
                    {
                        this.bringToBack();
                    }
                    befselected=this.options.myid;
                });


                // circle[this.id].on('dblclick', function (e) {

                //     map.removeLayer(this);
                //     return false;
                // });
                console.log("icon",this.lat, this.lon,ind);

                var myIcon = L.divIcon({className: 'my-div-icon-'+ind});
                var pstop=moment(this.stop + " "+z, "DD-MM-YYYY HH:mm:ss Z").isAfter();
                var sstop=moment(this.stop + " "+z, "DD-MM-YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss");
                console.log("sstop",sstop)
                if(this.time && pstop){

                    L.marker([this.lat, this.lon], {icon: myIcon}).addTo(map);
                    console.log("add timer",this.stop,moment(this.stop + " "+z, "DD-MM-YYYY HH:mm:ss Z").isAfter());
                    $('.my-div-icon-'+ind).addClass('my-div-icon');
                    $('.my-div-icon-'+ind).countdown(sstop)
                        .on('update.countdown', function (event) {
                            var totalHours = event.offset.totalDays * 24 + event.offset.hours;

                            $(this).html(event.strftime(totalHours + ':%M:%S'));
                            $('.my-div-icon-'+ind).css('width','unset');
                            $('.my-div-icon-'+ind).css('margin-left',-($('.my-div-icon-'+ind).width()/2));
                        })
                        .on('finish.countdown', function (event) {

                            switchCircle(table.getData());

                        });

                }
                console.log("hop");

            }
        });
        if (!selected) {
            selected = d[0];
        }
        ////console.log("lll",selected);
        if(selected)
        {
            map.panTo(new L.LatLng(selected.lat, selected.lon));
            var nrad = 20 - Math.log(selected.radius);

            map.setView([selected.lat, selected.lon], nrad);
        }
        if (cGroup) {
            // ////console.log('removing circle');


            //map.removeLayer(cGroup);
        }



    }
    $('.remove-user').on('click',function(e){

        e.preventDefault();
        event.stopPropagation();

        if (confirm(trans('basic.delete_user_cnf')+' #'+$(this).attr('data-uid')+' '+$(this).attr('data-email')+'?')) {
            window.location='/admin/removeu/'+$(this).attr('data-uid');
        } else {
            // Do nothing!
        }
    });
    $('.remove-track').on('click',function(e){

        e.preventDefault();
        event.stopPropagation();

        if (confirm(trans('basic.delete_track_cnf')+' #'+$(this).attr('data-tid')+' '+$(this).attr('data-title')+'?')) {
            window.location='/admin/removetrack/'+$(this).attr('data-tid');
        } else {
            // Do nothing!
        }
    });
    $('.edit-track,.edit-tracktitle').on('click',function(e){

        e.preventDefault();
        event.stopPropagation();

        window.location=$(this).attr('href');
    });
    $('.tracktitle').on('click',function(){

        //  alert($(this).attr('id').replace("track","trackc"));
        $('#'+$(this).attr('id').replace("track","trackc")).toggle();
        var id=$(this).attr('id').split("-");
        fetchtrack(id[1]);
    });


});


function fetchtrack(id){
    ////console.log("loading");
    var fetch = $.ajax({
        url: "/admin/getpoints/"+id,
        type: 'get',

        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        success: function (data) {
            ////console.log(data.data)
            ////console.log("putting in #trackc-"+id);
            if(!data.data.length){
                ////console.log("no");
                $('#trackc-'+id).html(trans('basic.no_points'));
            }
            else{
                ////console.log("yes");
                $('#trackc-'+id).html("");
                $('#trackc-'+id).append(pointstable(data.data));
                // $.each(data.data, function (ind, val) {
                //     $('#trackc-'+id).append(pointstable(val));
                // })


            }

        }
    });
}




var dateEditor = function(cell, onRendered, success, cancel, editorParams){
    var cellValue = cell.getValue(),
        input = document.createElement("input");
//console.log(cell);
    input.setAttribute("type", "text");

    input.style.padding = "4px";
    input.style.width = "100%";
    input.style.boxSizing = "border-box";

    input.value = typeof cellValue !== "undefined" ? cellValue : "";

    onRendered(function(){
        input.style.height = "100%";
        // //console.log("ggg");

        // $(input).attr('data-target','#datetimepicker5');
        // $(input).attr('id','datetimepicker5');
        // $(input).attr('data-toggle','datetimepicker');
        // $(input).addClass('datetimepicker-input');
        // $(input).addClass('form-control');
        // $('#datetimepicker5').datetimepicker(); //turn input into datepicker
        // //console.log( $('#datetimepicker5'));
        // input.focus();

        $('#myModal').modal('show');

        $('#datetimepicker13').on('change.datetimepicker',function(e){
            ////console.log("hhh",e.date);
            cell.setValue(e.date.format("DD-MM-YYYY HH:mm:ss"))

            success(e.date.format("DD-MM-YYYY HH:mm:ss"));
            //$('#myModal').modal('hide');

        });
    });

    function onChange(e){
        if(((cellValue === null || typeof cellValue === "undefined") && input.value !== "") || input.value != cellValue){
            success(input.value);
        }else{
            cancel();
        }
    }

    //submit new value on blur or change
    // input.addEventListener("change", onChange);
    // input.addEventListener("blur", onChange);

    // //submit new value on enter
    // input.addEventListener("keydown", function(e){
    //     switch(e.keyCode){
    //         case 13:
    //         success(input.value);
    //         break;

    //         case 27:
    //         cancel();
    //         break;
    //     }
    // });

    return input;
}
$(function () {
    $('#datetimepicker13').datetimepicker({
        inline: true,
        sideBySide: true,
        format: 'DD-MM-YYYY HH:mm'
    });



});

function pointstable(points){

    var res=`<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Title</th>
      <th scope="col">Start</th>
      <th scope="col">Stop</th>
      <th scope="col">Lat</th>
      <th scope="col">Lon</th>
      <th scope="col">Radius</th>
      <th scope="col">Timer</th>
    </tr>
  </thead>
  <tbody>`;

    $.each(points, function( index, value ) {



        res += `<tr>
      <th scope="row">`+(index+1)+`</th>
      <td>`+(value.title)+`</td>
      <td>`+(value.start)+`</td>
      <td>`+(value.stop)+`</td>
      <td>`+(value.lat)+`</td>
      <td>`+(value.lon)+`</td>
      <td>`+(value.radius)+`</td>
      <td>`+(value.time)+`</td>
    </tr>`;
    });
    res += `</tbody>
</table>`;
    return res;
}


$(document).ready(function() {

    $('#copy-button').tooltip();

    $('#copy-button').on('click', function() {
        var input = document.querySelector('#copy-input');
        input.focus();
        input.setSelectionRange(0, input.value.length + 1);
        try {
            var success = document.execCommand('copy');
            if (success) {
                $('#copy-button').trigger('copied', ['Copied!']);
            } else {
                $('#copy-button').trigger('copied', ['Use Ctrl+C']);
            }
        } catch (err) {
            //console.log(err);
            $('#copy-button').trigger('copied', ['Use Ctrl+C']);
        }
    });


    $('#copy-button').on('copied', function(event, message) {
        $(this).attr('title', message)
            .tooltip('_fixTitle')
            .tooltip('show')
            .attr('title', "Copy")
            .tooltip('_fixTitle');
    });

});


$('#myModal').on('hide.bs.modal', function () {
    $('#datetimepicker13').off('change.datetimepicker');
})
