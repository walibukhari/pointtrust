<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Point;
use App\Track;
use App\Http\Resources\Point as PointResource;
use Illuminate\Support\Facades\Auth;


class PointsController extends Controller
{
    function getpoints($id){

        $track=Track::where('id',$id)->first();
        if($track->user_id!=Auth::user()->id and Auth::user()->roles!=0){
            abort(404);
        }
        $points=Point::where('track',$id)->orderBy('id','asc')->get();
        
        return new PointResource($points);



    }
    function savepoints(Request $request, $id){
        $track=Track::where('id',$id)->first();
        if($track->user_id!=Auth::user()->id and Auth::user()->roles!=0){
            abort(404);
        }
        $ptodel= Point::where('track',$id);
        $ptodel->delete();
        $data=$request->input('data');
     
        array_walk($data, function (& $item,$k,$id) {
            
            $item['start'] = date("Y-m-d H:i:s", strtotime($item['start']));
            $item['stop'] = date("Y-m-d H:i:s", strtotime($item['stop'])); 
            $item['track'] = $id;
            if(isset( $item['time']))
            {
                $item['time'] = (int)filter_var( $item['time'], FILTER_VALIDATE_BOOLEAN );
            }
            else{
                $item['time'] = 0;
            }
            unset($item['loc']);
            unset($item['id']);
         }, $id);
       

       Point::insert($data);

    }
    function getpoint($id){
   
        $points=Point::where('track',$id)
                    ->where('start','<=', date('Y-m-d H:i:s',time()))
                    ->where('stop','>', date('Y-m-d H:i:s',time()))->get();

        return new PointResource($points);



    }
}
