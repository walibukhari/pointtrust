<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Track;
use App\User;
class TracksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function index(){
        if(Auth::user()->roles==0)
        {
            $u=User::with('tracks')->get();
        }
        else
        {
            $u=User::where('id',Auth::user()->id)->with('tracks')->get();
        }
        // dd($tracks);
        return view('admin', ['users'=>$u]);
    }
    function view($id){

        $track=Track::where('id',$id)->first();
        if($track->user_id!=Auth::user()->id and Auth::user()->roles!=0){
            abort(404);
        }


        return view('admintrack',["id"=>$id]);
    }
    function store(Request $request){
        $validatedData = $request->validate([
            
            'title' => ['required', 'string', 'max:50']
    
        ]);
        Track::create([
            'title' => $request->input('title'),
            'user_id' => Auth::user()->id
        ]);

        return redirect('/admin');
    }
    public function update(Request $request)
    {
        $validatedData = $request->validate([
            
            'title' => ['required', 'string', 'max:50']
    
        ]);
        $track = Track::findOrFail($request->input('id'));
        
        if($track->user_id!=Auth::user()->id and Auth::user()->roles!=0){
            abort(404);
        }
        $track->title=$request->input('title');
        $track->save();
        return redirect('/admin');
    }
    protected function removetrack($id)
    {
        $track = Track::find($id);
        
        if($track->user_id!=Auth::user()->id and Auth::user()->roles!=0){
            abort(404);
        }
        $track->delete();

        return redirect('/admin');
    }
}
