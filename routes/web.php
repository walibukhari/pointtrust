<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/track/{id}', 'FrontController@index');
// Route::get('/track/{id}', function () {
//     return view('frontpage');
// });
Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::get('/admin', 'TracksController@index')->name('home');
    Route::get('/admin/track/{id}', 'TracksController@view');
    Route::get('/admin/getpoints/{id}',  'PointsController@getpoints');
    Route::post('/admin/savepoints/{id}',  'PointsController@savepoints');

    Route::post('/admin/newtrack', 'TracksController@store')->name('newtrack');
    Route::post('/admin/edittrack', 'TracksController@update')->name('edittrack');


    Route::get('/admin/addtrack', function () {
        return view('addtrack');
    });
    Route::get('/admin/edittrack/{id}', 'FrontController@edit');
    Route::get('/admin/removetrack/{id}', 'TracksController@removetrack');


    Route::get('/admin/help', 'FrontController@help');
    
    
    
    
    

});

Route::group(['middleware' => ['auth', 'admin']], function(){
    Route::get('/admin/adduser', function () {
        return view('adduser');
    });
    Route::post('/admin/registeruser', 'Auth\UsersController@create')->name('registeruser');
    Route::get('/admin/removeu/{id}', 'Auth\UsersController@removeuser');
});
Route::get('/getpoint/{id}',  'PointsController@getpoint');
// Route::middleware('auth')->get('/track/{id}', 'FrontController@index');






// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/initsetup', function() {
    if(!Schema::hasTable('tracks'))
    {
        $output = [];
        \Artisan::call('config:clear', $output);
        \Artisan::call('key:generate', $output);
        \Artisan::call('migrate', $output);
        \Artisan::call('db:seed', $output);
        dd($output);
    }
});
Route::get('/clear-cache', function() {
    $output = [];
    \Artisan::call('cache:clear', $output);
    dd($output);
});