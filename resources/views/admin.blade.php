@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
        @if(Auth::user()->roles==0)
        <a href="/admin/adduser" class="btn btn-success">{{ __('basic.add_user') }}</a>
        @endif
        <a href="/admin/addtrack" class="btn btn-primary ml-2">{{ __('basic.add_track') }}</a>
    </div>

    @foreach ($users as $u)
    <div class="row justify-content-center pt-3">
        <div class="col-md-12">
            <div class="card{{$u->id==1?' border-primary ':''}}">

                <div class="card-header d-flex justify-content-between">{{$u->email}}
                    @if(Auth::user()->roles==0 && $u->id!=1)
                    <button class="btn btn-sm btn-danger ml-2 remove-user" data-uid="{{$u->id}}" data-email="{{$u->email}}">{{ __('basic.remove') }}</button>
                    @endif
                </div>

                <div class="card-body">


                    <ul class="list-group">
                        


                        @if(sizeof($u->tracks))
                        @foreach ($u->tracks as $track)

                        <li class="list-group-item list-group-item-action d-flex justify-content-between tracktitle" id="track-{{$track->id}}">
                            <div>
                                
                                    #{{$track->id}} {{$track->title}}
                                     </div>
                            <div>
                                <a href="/admin/edittrack/{{$track->id}}" class="btn btn-default edit-tracktitle btn-sm"><i class="fas fa-edit"></i></a>
                              
                                <a href="/admin/track/{{$track->id}}" class="btn btn-sm btn-primary edit-track">{{ __('basic.edit') }}</a>
                                <button class="btn btn-sm btn-warning ml-2 remove-track" data-tid="{{$track->id}}" data-title="{{$track->title}}">{{ __('basic.remove') }}</button>
                            </div>
                        </li>
                        <li class="list-group-item list-group-item-action disabled trackcontent" id="trackc-{{$track->id}}">
                            <p>{{ __('basic.loading') }}</p>
                        </li>
                        @endforeach
                        @else
                        <li class="list-group-item list-group-item-action text-center disabled">{{ __('basic.nothing_to_show') }}</li>
                        @endif


                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endforeach


</div>
@endsection
